package com.java.springboot.jwt;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.java.springboot.jwt.entity.User;
import com.java.springboot.jwt.repository.UserRepository;

@SpringBootApplication
public class SpringBootJwtApplication implements CommandLineRunner {
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UserRepository userRepository;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJwtApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		User user1=User
				.builder()
				.email("akshay@gmail.com")
				.username("akshay")
				.password(this.passwordEncoder.encode("akshay"))
				.role("ROLE_ADMIN")
				.build();
		User user2=User
				.builder()
				.email("ROHIT@gmail.com")
				.username("rohit")
				.password(this.passwordEncoder.encode("rohit"))
				.role("ROLE_USER")
				.build();
		
	//	this.userRepository.saveAll(List.of(user1,user2));
		
	}

}
