package com.java.springboot.jwt.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticatedController {
	
	@GetMapping("/authenticated")
	public String authenticatedMethod() {
		return "Hi I am Authorized";
	}
	

}
