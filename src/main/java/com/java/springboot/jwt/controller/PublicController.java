package com.java.springboot.jwt.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.java.springboot.jwt.dto.LoginRequestDto;
import com.java.springboot.jwt.dto.LoginResponseDto;
import com.java.springboot.jwt.service.CustomUserDetailsService;
import com.java.springboot.jwt.util.JwtUtil;

@RestController
public class PublicController {
	
	@Autowired
	private JwtUtil jwtUtil;
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	private AuthenticationProvider authenticationProvider;
	
	//private AuthenticationManager authenticationManager;
	
	@GetMapping("/public")
	public String publicMethod() {
		return "Hi I am public";
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody LoginRequestDto loginRequestDto) throws Exception {
		String token=null;
		String type="Bearer";
		System.out.println(loginRequestDto);
		try {
			//validate username and password
			this.authenticationProvider
			.authenticate(new UsernamePasswordAuthenticationToken(
					loginRequestDto.getUsername(), loginRequestDto.getPassword()));
		} catch (UsernameNotFoundException e) {
			throw new Exception("user not found");
		} catch (BadCredentialsException e) {
			throw new Exception("Invalid credentials:username/password");
			// TODO: handle exception
		}
		
		UserDetails userDetails= this.customUserDetailsService.loadUserByUsername(loginRequestDto.getUsername());
		//Generate token
		token = this.jwtUtil.generateToken(userDetails);
		Date expiryTime=this.jwtUtil.getExpirationDateFromToken(token);
		 LoginResponseDto loginResponseDto=LoginResponseDto
				    .builder()
					.token(token)
					.type(type)
					.expirtTime(expiryTime)
					.build();
		 return ResponseEntity.ok(loginResponseDto);
				
	}

}
