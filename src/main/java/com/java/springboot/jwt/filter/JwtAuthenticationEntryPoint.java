package com.java.springboot.jwt.filter;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.java.springboot.jwt.dto.JwtValidationErrorDto;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {

		JwtValidationErrorDto jwtValidationErrorDto = JwtValidationErrorDto.builder().code(401)
				.message("Unauthorized: Token is invalid/missing").build();

		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(jwtValidationErrorDto);
		response.setStatus(401);
		response.getWriter().print(json);
		response.setContentType("application/json");
	}

}
